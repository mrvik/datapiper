package util

import (
	"reflect"
	"strings"
)

// StructFields returns field names of a defined struct type.
func StructFields(of reflect.Type) []string {
	num := of.NumField()
	rt := make([]string, num)

	for i := 0; i < num; i++ {
		rt[i] = of.Field(i).Name
	}

	return rt
}

// StructFieldsTag returns field names using the provided tag.
// Tag is separated by "," and the first occurrence will be taken.
// If tag is not present, the field name will be returned.
func StructFieldsTag(of reflect.Type, tagName string) []string {
	num := of.NumField()
	rt := make([]string, num)

	for i := 0; i < num; i++ {
		field := of.Field(i)
		tag := strings.SplitN(field.Tag.Get(tagName), ",", 1)[0]
		name := tag

		switch tag {
		case "":
			name = field.Name
		case "_":
			name = ""
		}

		rt[i] = name
	}

	return rt
}

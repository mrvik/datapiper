# Data piper

Data piper is a CLI utility to run pipelines over your data.
It's core is based on generating named datasets and pass them from one stage to another. That's it.

The pipeline can be configured using a yaml file defining each stage with it's kind and configuration.

## Stage kinds

* source -> Read a file on one of the accepted formats
* transform -> Run a script over the current data (the script must be written on a language interpreted by a present engine)
* write -> Write current data to a file on one of the accepted formats
* debug -> Output current datasets

So a pipeline looks as follows:

```yaml
- name: start
  kind: source
  config:
    filename: my-file.xlsx  # A file with 2 sheets that we want to merge
- name: merge-tables
  kind: transform
  config:
    filename: merge-script.js  # Merge them with this script
- name: write json
  kind: write
  config:
    filter-datasets:  # Pass some datasets instead of the full map.
      - start_Sheet1
    filename: merged.json  # Write them to json
- name: write xlsx
  kind: write
  config:
    filename: merged.xlsx  # Write them back to xlsx
```

## Implemented formats

Formats allow reading and writing files (decode/encode):

* json
* yaml
* xlsx/xlsm/xlst (thanks to xuri/excelize)

## Implemented scripting languages

Transform stages run an script specified on `config.filename` and use the return value to create a new dataset.
Different scripting languages use different conventions in order to create an appropriate dataset type.

### JS

Files with `.js` extension are interpreted as Javascript and ran using [github.com/dop251/goja](https://github.com/dop251/goja).

* The script must define a function `stage` receiving an object with all datasets by name.
* Function must return an object or an array of objects.

Go functions are callable from the script as regular `js` functions.
`stage` should call `Map()` or `Struct()` on the dataset. Some `Dataset` implementations do not translate to `js` arrays so one of those functions should be called to get the real data.

You can use all the syntax recognized by `dop251/goja`. Currently it's all `ES5` and some `ES6`.

```javascript
// Add the current date to all elements.
const stage=data=>data["my_source_default"].Map().map(element=>({
    ...element,
    currentDate: new Date(),
}))
```

Function can return one of those values:

* A `Dataset` implementation.
* A JS array. You will be warned if some of the elements aren't objects.
* A JS object.
* An specific JS object with the following properties:
  * `__data`: A JS array of objects
  * `__keys`: A JS array of strings representing the object keys. This dataset will sort keys of all objects using this array.

As of ES5 and ES6 objects are unordered and `goja` converts JS objects into go maps, which don't have their keys ordered.
If you need an specific key order on this output, use the last return method described.

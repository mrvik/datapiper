module datapiper

go 1.16

require (
	github.com/dop251/goja v0.0.0-20210804101310-32956a348b49
	github.com/gabriel-vasile/mimetype v1.3.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/xuri/excelize/v2 v2.4.1 // indirect
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

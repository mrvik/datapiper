package script

import (
	"datapiper/formats"
	"errors"
	"fmt"

	"github.com/dop251/goja"
	"github.com/sirupsen/logrus"
)

type JSEngine struct{}

var (
	ErrBadResult = errors.New("got a bad result from script engine")
	ErrNoFn      = errors.New("no function defined on transform script")
)

func (JSEngine) RunStage(name string, script []byte, datasets map[string]formats.Dataset) (map[string]formats.Dataset, error) {
	vm := goja.New()
	log := logrus.WithFields(logrus.Fields{
		"script": name,
	})

	if _, err := vm.RunScript(name, string(script)); err != nil {
		return nil, fmt.Errorf("run startup program: %w", err)
	}

	var (
		multiDS      = vm.Get("multiDS")
		single       = vm.Get("stage")
		rt           goja.Value
		err          error
		resultParser = scanResult
	)

	if runFn, ok := goja.AssertFunction(multiDS); ok {
		resultParser = multiScanResult
		rt, err = runFn(goja.Undefined(), vm.ToValue(datasets))
	} else if runFn, ok := goja.AssertFunction(single); ok {
		rt, err = runFn(goja.Undefined(), vm.ToValue(datasets))
	} else {
		err = ErrNoFn
	}

	if err != nil {
		return nil, fmt.Errorf("run %q stage function: %w", name, err)
	}

	return resultParser(name, log, rt)
}

func multiScanResult(_ string, log *logrus.Entry, rt goja.Value) (map[string]formats.Dataset, error) {
	mp := make(map[string]formats.Dataset)

	switch exp := rt.Export().(type) {
	case map[string]interface{}:
		for name, content := range exp {
			ds, err := generateDataset(log.WithField("dataset", name), content)
			if err != nil {
				return nil, err
			}

			mp[name] = ds
		}
	case map[string]formats.Dataset:
		return exp, nil
	}

	return mp, nil
}

func scanResult(stage string, log *logrus.Entry, rt goja.Value) (map[string]formats.Dataset, error) {
	exp := rt.Export()
	log.Debugf("Returned from %q: %+v", stage, exp)
	ds, err := generateDataset(log, exp)
	if err != nil {
		return nil, err
	}

	log.Debugf("Returned ds on stage %q is %T: %+v", stage, ds, ds)

	return map[string]formats.Dataset{
		fmt.Sprintf("%s_script", stage): ds,
	}, nil
}

func generateDataset(log *logrus.Entry, from interface{}) (formats.Dataset, error) {
	var ds formats.Dataset

	switch exp := from.(type) {
	case formats.Dataset: // Perfect!
		ds = exp
	case []map[string]interface{}: // Extrange, but ok
		log.Warnf("ES6 objets are not required to follow any order, your keys will be random")

		ds = formats.GenericDataset(exp)
	case []interface{}: // JS Arrays created from there
		ds = formats.GenericFromArray(exp)
	case map[string]interface{}: // JS Object
		keys, okk := exp["__keys"].([]interface{})
		values, okv := exp["__data"].([]interface{})

		if okk && okv {
			var err error

			log.Infof("Script returned __keys and __values, using an StructuredDataset")

			if ds, err = structuredDS(keys, values); err != nil {
				return nil, err
			}
		} else {
			ds = formats.GenericDataset{exp}
		}
	default:
		return nil, fmt.Errorf("unexpected type %T: %w", exp, ErrBadResult)
	}

	return ds, nil
}

func structuredDS(keys, vls []interface{}) (formats.StructuredDataset, error) {
	strks := make([]string, len(keys))
	values := make([]map[string]interface{}, len(vls))

	for i, v := range keys {
		strks[i] = v.(string) // Must!
	}

	for i, v := range vls {
		values[i] = v.(map[string]interface{})
	}

	ds := formats.NewStructuredDataset(strks)

	if err := ds.Populate(values); err != nil {
		return ds, fmt.Errorf("populate dataset with script result: %w", err)
	}

	return ds, nil
}

package script

import (
	"datapiper/formats"
	"errors"
	"fmt"
	"io"
	"path"
)

type Engine interface {
	RunStage(name string, script []byte, datasets map[string]formats.Dataset) (map[string]formats.Dataset, error)
}

var engines = map[string]Engine{
	".js": JSEngine{},
}

var ErrNoEngine = errors.New("can't find required scripting engine")

func RunScriptStage(stage, basename string, reader io.Reader, fd formats.NamedSets) (formats.NamedSets, error) {
	engine, ok := engines[path.Ext(basename)]
	if !ok {
		return fd, fmt.Errorf("engine for %q: %w", basename, ErrNoEngine)
	}

	content, err := io.ReadAll(reader)
	if err != nil {
		return fd, fmt.Errorf("read script file: %w", err)
	}

	ds, err := engine.RunStage(stage, content, fd.CopySets())
	if err != nil {
		return fd, fmt.Errorf("exec script stage: %w", err)
	}

	fd.UpdateFrom(ds)

	return fd, nil
}

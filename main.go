package main

import (
	"datapiper/stages"
	"os"

	"github.com/sirupsen/logrus"
)

func main() {
	debug := os.Getenv("DATAPIPER_DEBUG") != ""

	if debug {
		logrus.SetLevel(logrus.DebugLevel)

		logrus.Debug("Starting with DEBUG=true")
	}

	if len(os.Args) != 2 {
		logrus.Fatalf("data-piper takes exactly one parameter (the yaml config file)")

		return
	}

	configPath := os.Args[1]

	stages, err := stages.NewFromConfig(configPath)
	if err != nil {
		logrus.Fatalf("read stages: %s", err)
	}

	if err := stages.Run(); err != nil {
		logrus.Fatalf("run stages: %s", err)
	}

	logrus.Debug("Done!")
}

package formats

import (
	"fmt"
	"reflect"

	"github.com/sirupsen/logrus"
)

type GenericDataset []map[string]interface{}

func (g GenericDataset) Map() []map[string]interface{} {
	return g
}

func (g GenericDataset) Struct() interface{} {
	if len(g) == 0 {
		return nil
	}

	var (
		sl            []interface{}
		interfaceType = reflect.TypeOf(sl).Elem()
		i             = 0
		keys          = make([]reflect.StructField, 0, len(g))
	)

	for k, v := range g[0] {
		keys = append(keys, reflect.StructField{ //nolint: exhaustivestruct
			Name: fmt.Sprintf("Field%d", i),
			Type: defaultType(v, interfaceType),
			Tag:  reflect.StructTag(fmt.Sprintf(`json:"%s" yaml:"%s" originalName:"%s"`, k, k, k)),
		})

		i++
	}

	typedef := reflect.StructOf(keys)
	rt := reflect.MakeSlice(reflect.SliceOf(typedef), len(g), len(g))

	for i, v := range g {
		tp := reflect.New(typedef).Elem()

		for ii, k := range keys {
			if val := v[k.Tag.Get("originalName")]; val != nil {
				tp.Field(ii).Set(reflect.ValueOf(val))
			}
		}

		rt.Index(i).Set(tp)
	}

	return rt.Interface()
}

func defaultType(v interface{}, df reflect.Type) reflect.Type {
	switch t := reflect.TypeOf(v); t {
	case nil:
		return df
	default:
		return t
	}
}

// Create a generic dataset from an array of map[string]interface{} not identified.
// Other values on the root are inserted in a __divergent key on first element.
func GenericFromArray(a []interface{}) GenericDataset {
	rt := make(GenericDataset, len(a))
	divergent := make(map[string]interface{})

	for i, v := range a {
		switch t := v.(type) {
		case map[string]interface{}:
			rt[i] = t
		default:
			divergent[fmt.Sprintf("key_%d", i)] = t
		}
	}

	if len(divergent) != 0 {
		logrus.Warn("There are lost elements in root element. Make sure your script is aware of this")

		rt = append(GenericDataset{divergent}, rt...)
	}

	return rt
}

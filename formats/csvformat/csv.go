package csvformat

import (
	"datapiper/formats"
	"encoding/csv"
	"fmt"
	"io"
)

func init() {
	formats.RegisterFormat(CSVFormatter{}, "text/csv", ".csv")
}

type CSVFormatter struct{}

func (CSVFormatter) Decode(name string, r io.Reader) (formats.NamedSets, error) {
	reader := csv.NewReader(r)
	ns := formats.NewNamedDS()

	headers, err := reader.Read()
	if err != nil {
		return ns, fmt.Errorf("read headers: %w", err)
	}

	ds := formats.NewStructuredDataset(headers)

	data, err := reader.ReadAll()
	if err != nil {
		return ns, fmt.Errorf("read fields: %w", err)
	}

	if err = ds.Populate(data); err != nil {
		return ns, fmt.Errorf("create dataset from csv entries: %w", err)
	}

	ns.AddSet(name, ds)

	return ns, nil
}

func (CSVFormatter) Encode(_ formats.NamedSets, _ io.Writer) error {
	panic("not implemented")
}

package encoderds

import (
	"datapiper/formats"
	"encoding/json"
	"fmt"
	"io"

	"gopkg.in/yaml.v3"
)

func init() {
	jsonFmt := EncoderFormat{
		E: func(r io.Writer) Encoder { return json.NewEncoder(r) },
		D: func(r io.Reader) Decoder { return json.NewDecoder(r) },
		N: "json",
	}

	yamlFmt := EncoderFormat{
		E: func(r io.Writer) Encoder { return yaml.NewEncoder(r) },
		D: func(r io.Reader) Decoder { return yaml.NewDecoder(r) },
		N: "yaml",
	}

	formats.RegisterFormat(jsonFmt, "application/json", ".json")
	formats.RegisterFormat(yamlFmt, "", ".yml", ".yaml")
}

type Encoder interface {
	Encode(interface{}) error
}

type Decoder interface {
	Decode(interface{}) error
}

type EncoderFormat struct {
	D func(io.Reader) Decoder
	E func(io.Writer) Encoder
	N string
}

func (ef EncoderFormat) Decode(name string, rd io.Reader) (formats.NamedSets, error) {
	var (
		ds = formats.NewNamedDS()
		gd interface{}
	)

	if err := ef.D(rd).Decode(&gd); err != nil {
		return ds, fmt.Errorf("create dataset from %s: %w", ef.N, err)
	}

	var finalData formats.Dataset

	switch v := gd.(type) {
	case map[string]interface{}:
		finalData = formats.GenericDataset([]map[string]interface{}{v})
	case []interface{}:
		finalData = formats.GenericFromArray(v)
	}

	ds.AddSet(name+"_default", finalData)

	return ds, nil
}

func (ef EncoderFormat) Encode(data formats.NamedSets, w io.Writer) error {
	if err := ef.E(w).Encode(data.PrintableSets()); err != nil {
		return fmt.Errorf("encode datasets to %s: %w", ef.N, err)
	}

	return nil
}

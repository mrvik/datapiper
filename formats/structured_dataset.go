package formats

import (
	"datapiper/util"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"

	"github.com/sirupsen/logrus"
)

var (
	// ErrNoType happens when there's a missing type annotation on a struct.
	ErrNoType = errors.New("missing type")
	// ErrBadType happens when the provided element has an invalid type.
	ErrBadType = errors.New("bad type received")
)

// StructuredDataset holds a slice of structs with the given ordered headers and interface{} values for each key.
// Make sure the headers start with uppercase. If some of them doesn't, readers/writers **must** ignore them.
type StructuredDataset struct {
	// data is sliceType value.
	data reflect.Value

	// Struct type held on data.
	structType reflect.Type
	// Slice type is []interface{} with interface{} being a value of structType.
	sliceType reflect.Type
}

func NewStructuredDataset(headers []string) StructuredDataset {
	sd := StructuredDataset{}
	sd.initType(headers)

	return sd
}

func (sd *StructuredDataset) initType(headers []string) {
	var interfaceValue interface{}

	interfaceType := reflect.TypeOf(&interfaceValue).Elem()
	fields := make([]reflect.StructField, len(headers))

	for i, name := range headers {
		fields[i] = reflect.StructField{ //nolint: exhaustivestruct
			Name: fmt.Sprintf("Field%d", i),
			Tag:  reflect.StructTag(fmt.Sprintf(`json:"%s" originalName:"%s" yaml:"%s"`, name, name, name)),
			Type: interfaceType,
		}
	}

	tp := reflect.StructOf(fields)
	sd.structType, sd.sliceType = tp, reflect.SliceOf(tp)
}

// AddRow adds an slice to the dataset. Data is assumed to be in the exact same order as the struct.
// data must be a slice or an error will be returned.
func (sd *StructuredDataset) AddRow(data interface{}) error {
	value := reflect.ValueOf(data)
	if value.Kind() != reflect.Slice {
		return fmt.Errorf("%w: expected an slice on AddRow", ErrBadType)
	}

	ln := value.Len()
	fieldsLen := sd.structType.NumField()
	st := reflect.New(sd.structType).Elem()
	target := ln

	if ln != fieldsLen {
		logrus.WithFields(logrus.Fields{
			"package": "formats",
			"dataset": "StructuredDataset",
			"method":  "AddRow",
		}).Warnf("Watch your result set, structure has %d fields and added record has %d", fieldsLen, ln)

		if fieldsLen < target {
			target = fieldsLen
		}
	}

	for i := 0; i < target; i++ {
		st.Field(i).Set(value.Index(i))
	}

	dt := sd.data
	if dt.Kind() == 0 {
		dt = reflect.MakeSlice(sd.sliceType, 0, 0)
	}

	sd.data = reflect.Append(dt, st)

	return nil
}

// Populate the structured dataset with data. This will use the type created on initialization
// and append all elements from the data slice into the current slice.
// data **must** be one of []map[string]interface{}, []struct{...}.
func (sd *StructuredDataset) Populate(data interface{}) error {
	if sd.structType == nil || sd.sliceType == nil {
		return fmt.Errorf("%w: no slice type present on Populate", ErrNoType)
	}

	insertData := reflect.ValueOf(data)
	if insertData.Kind() != reflect.Slice {
		return fmt.Errorf("%w: got %T instead of a slice on Populate", ErrBadType, data)
	}

	var insert []reflect.Value

	switch d := data.(type) {
	case []map[string]interface{}:
		insert = sd.prepareMap(d)
	default:
		insert = sd.prepareStructSlice(insertData)
	}

	currentData := sd.data
	if currentData.Kind() == 0 {
		currentData = reflect.MakeSlice(sd.sliceType, 0, 0)
	}

	sd.data = reflect.Append(currentData, insert...)

	return nil
}

func (sd StructuredDataset) prepareMap(mp []map[string]interface{}) []reflect.Value {
	rt := make([]reflect.Value, len(mp))
	fields := sd.originalStructFields()

	for i, v := range mp {
		s := reflect.New(sd.structType).Elem()

		for index, field := range fields {
			if v, ok := v[field]; ok {
				if v != nil {
					s.Field(index).Set(reflect.ValueOf(v))
				}
			}
		}

		rt[i] = s
	}

	return rt
}

func (sd StructuredDataset) structFields() []string {
	return util.StructFields(sd.structType)
}

func (sd StructuredDataset) originalStructFields() []string {
	return util.StructFieldsTag(sd.structType, "originalName")
}

func (sd StructuredDataset) prepareStructSlice(slice reflect.Value) []reflect.Value {
	ln := slice.Len()
	rt := make([]reflect.Value, ln)
	fields := sd.structFields()

	for i := 0; i < ln; i++ {
		dest := reflect.New(sd.structType).Elem()
		source := slice.Index(i)

		for _, field := range fields {
			if content := source.FieldByName(field); content.Kind() != 0 {
				dest.FieldByName(field).Set(content)
			}
		}

		rt[i] = dest
	}

	return rt
}

func (sd StructuredDataset) Map() []map[string]interface{} {
	if sd.data.Kind() == 0 {
		return nil
	}

	ln := sd.data.Len()
	rt := make([]map[string]interface{}, ln)
	fields := sd.originalStructFields()

	for i := 0; i < ln; i++ {
		mp := make(map[string]interface{})

		for index, field := range fields {
			mp[field] = sd.data.Index(i).Field(index).Interface()
		}

		rt[i] = mp
	}

	return rt
}

func (sd StructuredDataset) Struct() interface{} {
	return sd.data.Interface()
}

func (sd StructuredDataset) MarshalJSON() ([]byte, error) {
	return json.Marshal(sd.Struct()) //nolint: wrapcheck
}

package formats

import (
	"errors"
	"fmt"
	"io"
	"path"
	"sync"

	"github.com/gabriel-vasile/mimetype"
	"github.com/sirupsen/logrus"
)

type Format interface {
	Decode(string, io.Reader) (NamedSets, error)
	Encode(NamedSets, io.Writer) error
}

type NamedSets struct {
	m   map[string]Dataset
	mtx *sync.RWMutex
}

func NewNamedDS() NamedSets {
	return NamedSets{
		m:   make(map[string]Dataset),
		mtx: new(sync.RWMutex),
	}
}

func NamedDSMap(m map[string]Dataset) NamedSets {
	return NamedSets{
		m:   m,
		mtx: new(sync.RWMutex),
	}
}

func (fd NamedSets) AddSet(name string, content Dataset) {
	fd.mtx.Lock()
	defer fd.mtx.Unlock()

	fd.m[name] = content
}

func (fd NamedSets) GetSet(name string) (Dataset, bool) {
	fd.mtx.RLock()
	defer fd.mtx.RUnlock()

	ds, ok := fd.m[name]

	return ds, ok
}

func (fd NamedSets) CopySets(names ...string) map[string]Dataset {
	fd.mtx.RLock()
	defer fd.mtx.RUnlock()

	rt := make(map[string]Dataset)

	lnn := len(names)

	for name, ds := range fd.m {
		if lnn == 0 || someMatch(name, names) {
			rt[name] = ds
		}
	}

	return rt
}

func someMatch(name string, patterns []string) bool {
	for _, pattern := range patterns {
		switch m, err := path.Match(pattern, name); {
		case m:
			return true
		case err != nil:
			logrus.Errorf("Match %q againgst %q: %s", pattern, name, err)
		}
	}

	return false
}

func (fd NamedSets) PrintableSets() map[string]interface{} {
	fd.mtx.RLock()
	defer fd.mtx.RUnlock()

	rt := make(map[string]interface{}, len(fd.m))

	for k, v := range fd.m {
		rt[k] = v.Struct()
	}

	return rt
}

func (fd NamedSets) UpdateFrom(from map[string]Dataset) {
	for name, ds := range from {
		fd.AddSet(name, ds)
	}
}

type Dataset interface {
	Map() []map[string]interface{}
	Struct() interface{}
}

// Handler maps.
var (
	mimeHandlers      = map[string]Format{}
	extensionHandlers = map[string]Format{}
)

func RegisterFormat(format Format, mime string, extensions ...string) {
	if mime != "" {
		mimeHandlers[mime] = format
	}

	for _, e := range extensions {
		if e == "" || e[0] != '.' {
			panic("Invalid extension name: " + e)
		}

		extensionHandlers[e] = format
	}
}

// Errors.
var (
	ErrNoHandler = errors.New("handler not defined for this type")
)

func GetHandler(filename string, file io.ReadSeeker) (Format, error) {
	if file == nil {
		return detectByExtension(filename)
	}

	file.Seek(0, io.SeekStart)       //nolint: errcheck
	defer file.Seek(0, io.SeekStart) //nolint: errcheck

	detectedMIME, err := mimetype.DetectReader(file)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"filename": filename,
		}).Warnf("error detecting mime: %s", err)
	}

	if byMIME, ok := mimeHandlers[detectedMIME.String()]; ok {
		return byMIME, nil
	}

	return detectByExtension(filename)
}

func detectByExtension(name string) (Format, error) {
	if byExt, ok := extensionHandlers[path.Ext(name)]; ok {
		return byExt, nil
	}

	return nil, fmt.Errorf("%q(%s): %w", name, path.Ext(name), ErrNoHandler)
}

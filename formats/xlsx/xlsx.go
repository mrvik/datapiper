package xlsx

import (
	"datapiper/formats"
	"datapiper/util"
	"fmt"
	"io"
	"path"
	"reflect"

	"github.com/sirupsen/logrus"
	"github.com/xuri/excelize/v2"
)

// MaxTableRows set the limit for data tables.
// Exceeding this limit we could run out of memory.
const MaxTableRows = 200000

func init() {
	formats.RegisterFormat(XLSXFormat{}, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ".xlsx", ".xlsm", ".xlst")
}

type XLSXFormat struct{}

func (XLSXFormat) Decode(name string, r io.Reader) (formats.NamedSets, error) {
	data := formats.NewNamedDS()

	wb, err := excelize.OpenReader(r)
	if err != nil {
		return data, fmt.Errorf("open %q datasource: %w", path.Ext(name), err)
	}

	sheets := wb.GetSheetList()

	for _, s := range sheets {
		rs, err := wb.Rows(s)
		if err != nil {
			return data, fmt.Errorf("get cols on sheet %q: %w", s, err)
		}

		o, err := convertSheet(rs)
		if err != nil {
			return data, fmt.Errorf("structure sheet %q: %w", s, err)
		}

		data.AddSet(fmt.Sprintf("%s_%s", name, s), o)
	}

	return data, nil
}

func convertSheet(rs *excelize.Rows) (formats.StructuredDataset, error) {
	var (
		headersInitialized = false
		rt                 formats.StructuredDataset
	)

	for rs.Next() {
		content, err := rs.Columns()
		if err != nil {
			return rt, fmt.Errorf("read columns: %w", err)
		}

		if !headersInitialized {
			rt = formats.NewStructuredDataset(content)
			headersInitialized = true
		} else if err := rt.AddRow(content); err != nil {
			return rt, fmt.Errorf("add new row: %w", err)
		}
	}

	if e := rs.Error(); e != nil {
		return rt, fmt.Errorf("read rows: %w", e)
	}

	return rt, nil
}

type xlsxPage struct {
	name string
	data [][]interface{}
}

var defaultProperties = &excelize.DocProperties{
	Creator:        "datapiper",
	LastModifiedBy: "datapiper",
}

func (XLSXFormat) Encode(fd formats.NamedSets, w io.Writer) error {
	sets := fd.CopySets()
	pageChannel := make(chan xlsxPage, 2)

	go prepareData(sets, pageChannel)

	wb := excelize.NewFile()
	glog := logrus.WithFields(logrus.Fields{
		"package": "formats",
		"format":  "xlsx",
		"action":  "encode",
	})

	if err := wb.SetDocProps(defaultProperties); err != nil {
		glog.Warnf("Set document properties: %s", err)
	}

	for order := range pageChannel {
		wb.NewSheet(order.name)

		if len(order.data) == 0 {
			continue
		}

		log := glog.WithField("sheet", order.name)

		log.Debugf("Inserting %d rows", len(order.data))

		startAxis := "A1"
		endAxis, _ := excelize.CoordinatesToCellName(len(order.data[0]), len(order.data))

		for i1, row := range order.data {
			for i2, col := range row {
				cd, _ := excelize.CoordinatesToCellName(i2+1, i1+1)

				if err := wb.SetCellValue(order.name, cd, col); err != nil {
					return fmt.Errorf("set value on %q: %w", cd, err)
				}
			}
		}

		if len(order.data) > MaxTableRows {
			log.WithFields(logrus.Fields{
				"max-rows": MaxTableRows,
				"ds-rows":  len(order.data),
			}).Warnf("Dataset rows exceed max table rows")

			continue
		}

		log.Debugf("Creating new table for range '%s-%s'", startAxis, endAxis)

		tableStyle := `{
			"table_name": "table_` + order.name + `",
			"table_style": "TableStyleMedium1",
			"show_row_stripes": true
		}`

		if err := wb.AddTable(order.name, startAxis, endAxis, tableStyle); err != nil {
			log.WithField("range", startAxis+"-"+endAxis).Warnf("Error registering new data table: %s", err)
		}
	}

	wb.DeleteSheet(wb.GetSheetName(0))
	glog.Debugf("Writing workbook to %T", w)

	if _, err := wb.WriteTo(w); err != nil {
		return fmt.Errorf("write workbook: %w", err)
	}

	return nil
}

func prepareData(sets map[string]formats.Dataset, ch chan<- xlsxPage) {
	defer close(ch)

	for name, set := range sets {
		logrus.Debugf("Write dataset %T", set)

		slc := set.Struct()
		sliceValue := reflect.ValueOf(slc)
		elemType := reflect.TypeOf(slc).Elem()
		slcLen := sliceValue.Len()

		rows := make([][]interface{}, slcLen+1)
		fields := util.StructFields(elemType)
		rows[0] = make([]interface{}, len(fields))

		for i, v := range util.StructFieldsTag(elemType, "originalName") {
			rows[0][i] = v
		}

		logrus.WithFields(logrus.Fields{
			"dataset": name,
			"length":  slcLen,
		}).Debug("Mapping dataset to row slice")

		if slcLen == 0 {
			ch <- xlsxPage{name: name, data: rows}

			continue
		}

		for ei := 0; ei < slcLen; ei++ {
			value := sliceValue.Index(ei)
			row := make([]interface{}, len(fields))

			for i, field := range fields {
				row[i] = value.FieldByName(field).Interface()
			}

			rows[ei+1] = row
		}

		ch <- xlsxPage{name: name, data: rows}
	}
}

package stages

import (
	"datapiper/formats"
	"datapiper/script"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	// ErrBadConfig happens when an invalid config option is set.
	ErrBadConfig = errors.New("bad config specified")
	// ErrUnknownKind happens when an unregistered kind is specified.
	ErrUnknownKind = errors.New("unknown stage kind")
)

func (s Stages) Run() error {
	var (
		log         = logrus.WithField("package", "stages")
		ln          = len(s)
		start       = time.Now()
		currentData *formats.NamedSets
	)

	log.Debugf("Running %d stages", ln)

	for num, stage := range s {
		sub := time.Now()
		log := log.WithFields(logrus.Fields{
			"stage":   stage.Name,
			"kind":    stage.Kind,
			"current": fmt.Sprintf("%d/%d", num+1, ln),
		})

		log.Debugf("Running stage")

		cd, err := stage.Run(log, currentData)
		if err != nil {
			return fmt.Errorf("error on stage %s(%d): %w", stage.Name, num, err)
		}

		currentData = cd

		log.Infof("Ran stage on %d milliseconds", time.Since(sub).Milliseconds())
	}

	log.Infof("Ran %d stages on %d milliseconds", ln, time.Since(start).Milliseconds())

	return nil
}

func (s Stage) Run(log *logrus.Entry, current *formats.NamedSets) (*formats.NamedSets, error) {
	if s.Config.Filename == "" && s.Kind != KindDebug {
		return nil, fmt.Errorf("no filename on a %q stage: %w", s.Kind, ErrBadConfig)
	}

	if s.Kind != KindSource && current == nil {
		return nil, fmt.Errorf("there is nothing to write on a dest stage: %w", ErrBadConfig)
	}

	var passDatasets formats.NamedSets

	if current != nil {
		passDatasets = formats.NamedDSMap(current.CopySets(s.Config.FilterDatasets...))
	}

	basename := path.Base(s.Config.Filename)

	switch s.Kind {
	case KindSource:
		file, err := os.Open(s.Config.Filename)
		if err != nil {
			return nil, fmt.Errorf("read file on a source stage: %w", err)
		}

		defer file.Close()

		fm, err := formats.GetHandler(basename, file)
		if err != nil {
			return nil, fmt.Errorf("find handler: %w", err)
		}

		d, err := fm.Decode(s.Name, file)
		if err != nil {
			return nil, fmt.Errorf("call handler: %w", err)
		}

		if current == nil {
			current = &d
		} else {
			current.UpdateFrom(d.CopySets())
		}

	case KindDest:
		fm, err := formats.GetHandler(basename, nil)
		if err != nil {
			return nil, fmt.Errorf("get write handler: %w", err)
		}

		file, err := os.OpenFile(s.Config.Filename, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o600)
		if err != nil {
			return nil, fmt.Errorf("open dest file: %w", err)
		}

		defer file.Close()

		if err := fm.Encode(passDatasets, file); err != nil {
			return nil, fmt.Errorf("encode data into current file")
		}

		if err := file.Close(); err != nil {
			return nil, fmt.Errorf("close written file: %w", err)
		}
	case KindDebug:
		json.NewEncoder(os.Stderr).Encode(passDatasets.PrintableSets())
	case KindTransform:
		file, err := os.Open(s.Config.Filename)
		if err != nil {
			return nil, fmt.Errorf("open script file: %w", err)
		}

		defer file.Close()

		result, err := script.RunScriptStage(s.Name, basename, file, passDatasets)
		if err != nil {
			return nil, fmt.Errorf("exec transform stage: %w", err)
		}

		current.UpdateFrom(result.CopySets())
	default:
		return current, fmt.Errorf("%w: %q", ErrUnknownKind, s.Kind)
	}

	return current, nil
}

package stages

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type Stages []Stage

type Stage struct {
	Name string
	Kind string

	Config StageConfig
}

type StageConfig struct {
	// Filter datasets to be passed to this stage.
	FilterDatasets []string `yaml:"filter-datasets"` //nolint: tagliatelle

	Filename string
}

func NewFromConfig(filepath string) (Stages, error) {
	var stg Stages

	file, err := os.Open(filepath)
	if err != nil {
		return nil, fmt.Errorf("open config: %w", err)
	}

	defer file.Close()

	if err := yaml.NewDecoder(file).Decode(&stg); err != nil {
		return nil, fmt.Errorf("decode config file: %w", err)
	}

	return stg, nil
}

// Stage kinds.
const (
	KindSource    = "source"
	KindDest      = "write"
	KindTransform = "transform"
	KindDebug     = "debug"
)

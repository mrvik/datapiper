package main

// Import all source formats.
import (
	_ "datapiper/formats/csvformat"
	_ "datapiper/formats/encoderds"
	_ "datapiper/formats/xlsx"
)
